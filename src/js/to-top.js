$( document ).ready(function() {
    $('#to-top').fadeOut();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {
                $('#to-top').fadeIn();
            } else {
                $('#to-top').fadeOut();
            }
        });
        $('#to-top').click(function () {
            $('body,html').animate({scrollTop: 0}, 800);
        });
    });
});