$(document).ready(function () {
    $('.header-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        infinite: true,
        responsive: [
            {
                breakpoint: 601,
                settings: {
                    dots: false
                }
            }
        ]
    })
});